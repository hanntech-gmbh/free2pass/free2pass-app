### Added 

- Version information for web 
- Crypto concept for contact tracing without health department
- Check-In for private Occasions
- Creation of private Occasions
- Deep links to app
- Friendly notice for pentesters
- Dialog on max registration attempts

### Changed

- Reverted internally modified dependency `flutter_map_location` to the upstream now patched
- Renamed GitLab entrypoint config for web
- Flutter engine version used
- Migrated to null safety. Blocking: [flutter_map#829](https://github.com/fleaflet/flutter_map/issues/829)
- Expiring focus from inputs after activating other UI elements

### Deprecated

- Use of `package_info`

### Fixed

- Home page layout on tablet-size devices 
- Fixed overflow on pin verification view
- iOS preventing from entering a `-` in names
- Cleaned source code
- Certificate countdown timer for certificates with a validity of more than 24 hours

### Security

- Added random entropy to all encrypted personal data to avoid binary pattern detection
- Allow signature verification fo PDF-exported certificates