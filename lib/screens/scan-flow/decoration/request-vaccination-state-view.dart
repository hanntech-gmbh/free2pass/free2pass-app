import 'package:flutter/material.dart';
import 'package:free2pass/components/push-enabler-tile.dart';
import 'package:free2pass/enums/vaccination-state.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/push/push-adapter.dart';
import 'package:free2pass/screens/scan-flow/decoration/scan-result-view.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass_support/free2pass_support.dart';

class RequestVaccinationStateView extends StatefulWidget {
  final ValueChanged<VaccinationRequestData> onSelect;

  const RequestVaccinationStateView({Key? key, required this.onSelect})
      : super(key: key);

  @override
  _RequestVaccinationStateViewState createState() =>
      _RequestVaccinationStateViewState();
}

class _RequestVaccinationStateViewState
    extends State<RequestVaccinationStateView> {
  VaccinationState? _currentlySelected;

  bool? _receivePush;

  @override
  void initState() {
    if (getCurrentUser()
        .hasValidVaccinationCertificate(vaccinationCertificates)) {
      _currentlySelected = VaccinationState.IMMUNIZED;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScanResultView(
      children: [
        AlignedMarkdown(
            data: '# ' +
                S.of(context).vaccinatedQuestion +
                '\n' +
                S.of(context).vaccinatedQuestionExplanation),
        Padding(
          padding: MediaQuery.of(context).size.width > 350
              ? (EdgeInsets.only(left: MediaQuery.of(context).size.width / 6))
              : EdgeInsets.zero,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              RadioListTile(
                title: AlignedMarkdown(
                    data: S.of(context).vaccinationStateNoneLong),
                groupValue: _currentlySelected,
                value: VaccinationState.NONE,
                onChanged: _saveState,
              ),
              RadioListTile(
                title: AlignedMarkdown(
                    data: S.of(context).vaccinationStatePartialLong),
                groupValue: _currentlySelected,
                value: VaccinationState.PARTIAL,
                onChanged: _saveState,
              ),
              RadioListTile(
                title: AlignedMarkdown(
                    data: S.of(context).vaccinationStateImmunizedLong),
                groupValue: _currentlySelected,
                value: VaccinationState.IMMUNIZED,
                onChanged: _saveState,
              ),
              Divider(),
              RadioListTile(
                title: AlignedMarkdown(data: S.of(context).noInformation),
                groupValue: _currentlySelected,
                value: VaccinationState.UNKNOWN,
                onChanged: _saveState,
              ),
              if (!PushAdapter.instance.isDummy) Divider(),
              if (!PushAdapter.instance.isDummy)
                PushEnablerTile(onChanged: _handlePush),
              ElevatedButton(
                onPressed: _currentlySelected != null &&
                        (PushAdapter.instance.isDummy || _receivePush != null)
                    ? _submit
                    : null,
                child: Text(S.of(context).next),
              )
            ]
                .map((e) => Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: e,
                    ))
                .toList(),
          ),
        )
      ],
    );
  }

  void _saveState(VaccinationState? value) {
    setState(() {
      _currentlySelected = value;
    });
  }

  void _submit() {
    widget.onSelect(VaccinationRequestData(
      vaccinationState: _currentlySelected!,
      receivePush: _receivePush,
    ));
  }

  void _handlePush(bool? value) {
    setState(() {
      _receivePush = value;
    });
  }
}

class VaccinationRequestData {
  final VaccinationState? vaccinationState;
  final bool? receivePush;

  VaccinationRequestData({this.vaccinationState, this.receivePush});
}
