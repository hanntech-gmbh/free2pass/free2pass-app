import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:free2pass/components/dark-mode-overlay.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/screens/scan-flow/flows/certificate.dart';
import 'package:free2pass_support/free2pass_support.dart';

class ScanResultView extends StatelessWidget {
  final List<Widget> children;

  const ScanResultView({Key? key, required this.children}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      children: [
        Container(
          child: Transform.scale(
            scale: 1.375,
            child: Padding(
              padding: EdgeInsets.only(
                  right: MediaQuery.of(context).size.width * 0.125),
              child: DarkModeOverlay(
                child: SvgPicture.asset(
                  kNewCertificatePendingBackground,
                  excludeFromSemantics: true,
                ),
              ),
            ),
          ),
        ),
        SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: [
              Navigator.of(context).canPop()
                  ? Container(
                      alignment: Alignment.centerLeft,
                      child: IconButton(
                        padding: const EdgeInsets.all(16),
                        tooltip: S.of(context).close,
                        icon: Icon(
                          Icons.close,
                          color: Theme.of(context).primaryColor,
                        ),
                        onPressed: Navigator.of(context).pop,
                      ),
                    )
                  : Container(),
              Flexible(
                flex: 3,
                child: ResponsiveBox(
                  child: Container(
                    alignment: Alignment.centerRight,
                    padding: const EdgeInsets.symmetric(horizontal: 32.0),
                    child: DefaultTextStyle.merge(
                      style: TextStyle(color: gray, fontSize: 14),
                      textAlign: TextAlign.end,
                      child: Material(
                        color: Colors.transparent,
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisSize: MainAxisSize.min,
                            children: children,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Flexible(
                // on small devices, the bottom is cut otherwise
                flex: (MediaQuery.of(context).size.height /
                            MediaQuery.of(context).textScaleFactor >
                        786)
                    ? 1
                    : 0,
                child: Container(),
              )
            ],
          ),
        ),
      ],
    );
  }
}
