import 'package:after_layout/after_layout.dart';
import 'package:animations/animations.dart';
import 'package:crypto/crypto.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/dialogs/show-update/show-update.dart';
import 'package:free2pass/enums/appointments.enum.dart';
import 'package:free2pass/enums/certificate-status.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/scan-route-data.dart';
import 'package:free2pass/models/update-data.dart';
import 'package:free2pass/push/exchange/push-entrypoint.dart';
import 'package:free2pass/push/push-adapter.dart';
import 'package:free2pass/screens/home/home.dart';
import 'package:free2pass/screens/introduction/introduction.dart';
import 'package:free2pass/screens/recent-locations/recent-locations.screen.dart';
import 'package:free2pass/screens/scan-flow/scan-flow.dart';
import 'package:free2pass/screens/setup/setup.dart';
import 'package:free2pass/utils/app_config.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass/utils/migrations.dart';
import 'package:free2pass/utils/update-certificate-in-database.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:uni_links/uni_links.dart';

/// The asset location of the app's logo
const kLogoImage = kScalableAssetBase + 'bildmarke.svg';

/// The [SplashScreen] ist the entrypoint of the application.
///
/// Her we verify whether a user is logged in, whether we should show
/// the introduction pages and whether the user data (the selfie in particular)
/// has been modified.
class SplashScreen extends StatefulWidget {
  static const routeName = '/';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with AfterLayoutMixin {
  @override
  void initState() {
    _initializeLocalisation();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: SvgPicture.asset(kLogoImage),
              constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height / 2,
                maxWidth: MediaQuery.of(context).size.width / 2,
              ),
            ),
            Transform.scale(
              scale: 3,
              child: MarkdownBody(data: '## free2**pass**'),
            ),
            Padding(
              padding: const EdgeInsets.all(64.0),
              child: CircularProgressIndicator(),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // running after first layout as inherited widget is contacted
    _checkInitialized();
  }

  Future<void> _checkInitialized() async {
    final languageCode = settings.get(kLocaleKey, defaultValue: 'de');
    apiUrl = AppConfig.of(context)!.source == Stores.GitLabNightly
        ? betaApiUrl
        : productionApiUrl;
    await F2PApi.initialize(languageCode: languageCode);

    await PushAdapter.registerAdapter(AppConfig.of(context)!.pushAdapter);

    final hasOldUserData = (data.get(kPhoneNumberKey) != null &&
            data.get(kFullNameKey) != null &&
            data.get(kBirthdayKey) != null &&
            data.get(kPhotoLocationKey) != null &&
            ((data.get(kPhotoLocationKey) == false) ^
                (data.get(kPhotoChecksumKey) != null)) &&
            data.get(kIntroductionShown, defaultValue: false) &&
            data.get(kTownKey) != null &&
            data.get(kZipKey) != null &&
            data.get(kBirthdayKey) != null &&
            data.get(kStreetKey) != null) &&
        // in case the old data scheme is used, the [kCurrentUserKey] may not be [null]
        data.get(kCurrentUserKey) == null;

    // migrating to multi user
    if (hasOldUserData) {
      await migrateTemporaryUserDataToDatabase();
      for (var certificate in certificates.values) {
        await updateCertificateInDatabase(certificate);
      }
    }
    // either full user(s) or partial user data must be provided
    bool initialized = users.isNotEmpty;

    if (initialized) await runMigrationJobs();

    await defaultUserMigration();

    try {
      // checking photo check sums to avoid modification
      if (initialized) {
        for (var user in users.values) {
          // skip check sum verification in case no photo is provided
          if (user.photoLocation != null) {
            // checking whether image has been manipulated
            final image = await FilePickerCross.fromInternalPath(
                path: user.photoLocation!);

            final _currentUserImage = image.toUint8List();

            // initialized = initialized &&
            //     (sha256.convert(_currentUserImage).toString() ==
            //         (user.photoCheckSum));

            // in case this is the currently active user, we save the image for further use
            await user.hashInitialized;
            userImages[user.hash!] = _currentUserImage;
          }
        }
      }
    } catch (e) {
      initialized = false;
    }

    _cleanOldData();

    _checkForUpdates();

    // There was a bug where the phone number got lost after editing the main user. Check if we lost the phone number
    if (initialized && getDefaultUserData().phone == null) {
      print('NO PHONE NUMBER KNOWN!');
      await showModal(
          context: context,
          builder: (context) => AlertDialog(
                title: Text(S.of(context).phoneNumberLostTitle),
                content: Text(S.of(context).phoneNumberLostDescription),
                actions: [
                  TextButton(
                      onPressed: () => Navigator.of(context).pop(true),
                      child: Text(S.of(context).okay)),
                ],
              ));

      final defaultUser = getDefaultUserData();
      await data.put(kFullNameKey, defaultUser.name);
      await data.put(kBirthdayKey, defaultUser.birthday);
      if (defaultUser.email != null)
        await data.put(kEmailKey, defaultUser.email);
      await data.put(kStreetKey, defaultUser.street);
      await data.put(kTownKey, defaultUser.town);
      await data.put(kZipKey, defaultUser.zip);
      if (defaultUser.photoLocation != null &&
          defaultUser.photoCheckSum != null) {
        await data.put(kPhotoLocationKey, defaultUser.photoLocation);
        await data.put(kPhotoChecksumKey, defaultUser.photoCheckSum);
      }

      // make sure its really not known and delete it
      await data.delete(kPhoneNumberKey);
      initialized = false;
    }

    if (initialized) {
      Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);

      // checking whether notification opened app
      final entrypoint = await PushAdapter.instance.initialMessage();
      _handlePushEntrypoint(entrypoint);

      // handling future push notifications while the app is inactive
      PushAdapter.instance.messagesOpeningApp().forEach(_handlePushEntrypoint);

      // handling deep links now
      try {
        final initialLink = await getInitialUri();
        // in case we are dealing with a deep link containing a Crypto QR,
        // opening the corresponding page
        if (initialLink != null &&
            initialLink.host == 'go.free2pass.de' &&
            initialLink.hasFragment) {
          final handle = initialLink.fragment;

          // handling merchant and occasion check ins
          if (handle.startsWith('/checkIn')) {
            final data = handle.replaceFirst('/checkIn/', '');
            Navigator.of(context).pushNamed(
              ScanFlowScreen.routeName,
              arguments: ScanRouteData(
                  deeplink: data, initialScreen: ScanType.CHECKIN),
            );
          }
          // handling covid 19 tests
          else if (handle.startsWith('/test')) {
            final data = handle.replaceFirst('/test/', '');
            Navigator.of(context).pushNamed(
              ScanFlowScreen.routeName,
              arguments: ScanRouteData(
                deeplink: data,
                initialScreen: ScanType.CERTIFICATE,
              ),
            );
          }
        }
      } catch (e) {}
    } else {
      if (data.get(kIntroductionShown, defaultValue: false) == false) {
        Navigator.of(context)
            .pushReplacementNamed(IntroductionScreen.routeName);
      } else {
        Navigator.of(context).pushReplacementNamed(SetupScreen.routeName);
      }
    }
  }

  void _initializeLocalisation() {
    if (settings.containsKey(kLocaleKey)) {
      final String code = settings.get(kLocaleKey);
      switch (code) {
        case 'de':
          S.load(Locale(code));
          break;
        case 'en':
          S.load(Locale(code));
          break;
      }
    }
  }

  /// asks our server whether a newer app version is available and whether an update should be forced
  Future _checkForUpdates() async {
    if (kIsWeb) return;
    Stores source = AppConfig.of(context)!.source;

    final packageInfo = await PackageInfo.fromPlatform();

    final version = int.parse(packageInfo.buildNumber);

    final response = await F2PApi.instance.auth.shouldUpdate(
      store: source,
      version: version,
    );
    if (response.status.success &&
        response.payload!.versionAvailable! > version)
      showUpdateDialog(
        context,
        source: source,
        forceUpdate: response.payload!.forceUpdate!,
      );
  }

  void _handlePushEntrypoint(PushEntrypoint entrypoint) {
    if (entrypoint != PushEntrypoint.NONE) {
      if (entrypoint == PushEntrypoint.APPOINTMENT)
        Navigator.of(context).pushNamed(RecentLocationsScreen.routeName);
      if (entrypoint == PushEntrypoint.CERTIFICATE)
        Navigator.of(context).pushNamed(HomeScreen.routeName);
    }
  }
}

/// removes every [Certificate] or [Merchant] older than 14 days as well as
/// every [Appointment] older than 30 minutes
void _cleanOldData() {
  certificates.keys.forEach((element) {
    if (certificates
            .get(element)
            ?.resultDateTime
            .isBefore(DateTime.now().subtract(Duration(days: 14))) ??
        false) certificates.delete(element);
  });
  visitedLocations.keys.forEach((element) {
    if (visitedLocations
            .get(element)
            ?.checkInDateTime
            ?.isBefore(DateTime.now().subtract(Duration(days: 14))) ??
        false) visitedLocations.delete(element);
  });
  if (appointments.isNotEmpty) {
    for (int i = appointments.length - 1; i >= 0; i--) {
      final handle = appointments.getAt(i)!;
      if (DateTime.fromMillisecondsSinceEpoch(handle.startTime * 1000)
          .isBefore(DateTime.now().subtract(Duration(minutes: 30))))
        appointments.deleteAt(i);
    }
  }
  pushSubscriptions.values.forEach((element) {
    switch (element.type) {
      case PushEntrypoint.APPOINTMENT:
        final appointment = appointments.get(element.identifier);
        if (!(appointment != null &&
            appointment.state == AppointmentState.APPROVED &&
            !DateTime.fromMillisecondsSinceEpoch(appointment.startTime * 1000)
                .isBefore(DateTime.now().subtract(Duration(minutes: 30)))))
          pushSubscriptions.delete(element.identifier);
        break;

      case PushEntrypoint.CERTIFICATE:
        final certificate = certificates.get(element.identifier);
        if (!(certificate != null &&
            certificate.expectedValidity != CertificateStatusValidity.PENDING))
          pushSubscriptions.delete(element.identifier);
        break;
      default:
        break;
    }
  });
  if (pushSubscriptions.isEmpty) PushAdapter.instance.randomize();
}
