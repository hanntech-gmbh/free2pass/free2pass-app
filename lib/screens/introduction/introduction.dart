import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/screens/introduction/introduction-screen-card/introducation-screen-card.dart';
import 'package:free2pass/screens/splash/splash.dart';
import 'package:free2pass/utils/hive.util.dart';
import 'package:free2pass_support/free2pass_support.dart';

class IntroductionScreen extends StatefulWidget {
  static const routeName = '/introduction';

  @override
  _IntroductionScreenState createState() => _IntroductionScreenState();
}

String introAssets(int index) => 'assets/intro/intro-${index + 1}.svg';

class _IntroductionScreenState extends State<IntroductionScreen> {
  late AxisTransitionController _controller;

  @override
  void initState() {
    _controller = AxisTransitionController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        child: SafeArea(
          child: Container(
            height: 64,
            child: ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: [
                _controller.current != 0
                    ? MaterialButton(
                        onPressed: _previous,
                        child: Text(S.of(context).back),
                      )
                    : Container(),
                MaterialButton(
                  onPressed: _next,
                  child: Text(S.of(context).next),
                ),
              ],
            ),
          ),
        ),
      ),
      body: AxisTransitionView(
        backgroundColor: introBackground,
        controller: _controller,
        children: [
          IntroductionScreenCard(
            key: ValueKey(0),
            onAbort: _goBack,
            head: S.of(context).intro1Head,
            asset: introAssets(0),
            bottom: S.of(context).intro1Bottom,
          ),
          IntroductionScreenCard(
            key: ValueKey(1),
            onAbort: _goBack,
            head: S.of(context).intro2Head,
            asset: introAssets(1),
            bottom: S.of(context).intro2Bottom,
          ),
          IntroductionScreenCard(
            key: ValueKey(2),
            onAbort: _goBack,
            head: S.of(context).intro3Head,
            asset: introAssets(2),
            bottom: S.of(context).intro3Bottom,
          ),
          IntroductionScreenCard(
            key: ValueKey(3),
            onAbort: _goBack,
            head: S.of(context).intro4Head,
            asset: introAssets(3),
            bottom: S.of(context).intro4Bottom,
          ),
        ],
      ),
    );
  }

  void _goBack() {
    data.put(kIntroductionShown, true);
    if (Navigator.of(context).canPop())
      Navigator.of(context).pop();
    else
      Navigator.of(context).pushReplacementNamed(SplashScreen.routeName);
  }

  void _next() {
    if (_controller.current < 3) {
      setState(() {
        _controller.next();
      });
    } else
      _goBack();
  }

  void _previous() {
    setState(() {
      _controller.previous();
    });
  }
}
