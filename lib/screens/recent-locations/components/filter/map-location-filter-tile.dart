import 'package:flutter/material.dart';
import 'package:free2pass/enums/location-types.enum.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/location.dart';
import 'package:free2pass/models/map-locations-filter.dart';
import 'package:hive_flutter/hive_flutter.dart';

class MapLocationFilterTile extends StatefulWidget {
  final MapLocationFilter filter;
  final ValueChanged<MapLocationFilter> onChanged;

  const MapLocationFilterTile(
      {Key? key, required this.onChanged, required this.filter})
      : super(key: key);

  @override
  State<MapLocationFilterTile> createState() => _MapLocationFilterTileState();
}

class _MapLocationFilterTileState extends State<MapLocationFilterTile> {
  TextEditingController _searchController = TextEditingController();
  FocusNode _inputFocus = FocusNode();

  @override
  void initState() {
    if (widget.filter.query != null)
      _searchController.text = widget.filter.query!;
    _searchController.addListener(() {
      _handleSearch();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            title: Text(
              S.of(context).filterLocations,
              style: Theme.of(context).textTheme.headline5,
            ),
          ),
          Divider(),
          ListTile(
            title: TextField(
              autofocus: true,
              focusNode: _inputFocus,
              controller: _searchController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.search),
                suffixIcon: IconButton(
                  tooltip: S.of(context).clear,
                  icon: Icon(Icons.clear),
                  onPressed: _searchController.clear,
                ),
                labelText: S.of(context).search,
              ),
            ),
          ),
          ValueListenableBuilder(
              valueListenable: cachedLocations.listenable(),
              builder: (context, Box<Location> box, f) {
                return ListTile(
                  title: Text(S.of(context).matchingLocations +
                      ': ' +
                      box.values
                          .where((element) => widget.filter.matches(element))
                          .length
                          .toString()),
                );
              }),
          Divider(),
          ListTile(
            title: Text(
              S.of(context).locationTypes,
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
          CheckboxListTile(
            title: Text(S.of(context).issuers),
            value: widget.filter.types.isEmpty ||
                widget.filter.types.contains(LocationType.ISSUER),
            onChanged: _handleTypeIssuer,
          ),
          CheckboxListTile(
            title: Text(S.of(context).merchants),
            value: widget.filter.types.isEmpty ||
                widget.filter.types.contains(LocationType.MERCHANT),
            onChanged: _handleTypeMerchant,
          ),
          CheckboxListTile(
            title: Text(S.of(context).vaccinationStations),
            value: widget.filter.types.isEmpty ||
                widget.filter.types.contains(LocationType.VACCINATOR),
            onChanged: _handleTypeVaccinator,
          ),
          Divider(),
          SwitchListTile(
            value: widget.filter.active ?? true,
            title: Text(S.of(context).onlyShowFree2passenablesLocations),
            onChanged: _handleActive,
          ),
          Divider(),
          ButtonBar(
            children: [
              ElevatedButton(
                onPressed: _resetFilters,
                child: Text(S.of(context).resetFilters),
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  void didUpdateWidget(covariant MapLocationFilterTile oldWidget) {
    if (oldWidget.filter != widget.filter) setState(() {});
    super.didUpdateWidget(oldWidget);
  }

  void _resetFilters() {
    _inputFocus.unfocus();
    _searchController.clear();
    widget.onChanged(MapLocationFilter());
  }

  void _handleTypeIssuer(bool? value) {
    _inputFocus.unfocus();
    final Set<LocationType> types = Set.from(widget.filter.types);
    if (value == true)
      types.add(LocationType.ISSUER);
    else {
      if (types.isEmpty) {
        types.addAll(LocationType.values);
      }
      types.remove(LocationType.ISSUER);
    }

    widget.onChanged(widget.filter.copyWith(types: types.toList()));
  }

  void _handleTypeMerchant(bool? value) {
    _inputFocus.unfocus();
    final Set<LocationType> types = Set.from(widget.filter.types);
    if (value == true)
      types.add(LocationType.MERCHANT);
    else {
      if (types.isEmpty) {
        types.addAll(LocationType.values);
      }
      types.remove(LocationType.MERCHANT);
    }

    widget.onChanged(widget.filter.copyWith(types: types.toList()));
  }

  void _handleTypeVaccinator(bool? value) {
    _inputFocus.unfocus();
    final Set<LocationType> types = Set.from(widget.filter.types);
    if (value == true)
      types.add(LocationType.VACCINATOR);
    else {
      if (types.isEmpty) {
        types.addAll(LocationType.values);
      }
      types.remove(LocationType.VACCINATOR);
    }

    widget.onChanged(widget.filter.copyWith(types: types.toList()));
  }

  void _handleActive(bool value) {
    _inputFocus.unfocus();
    widget.onChanged(widget.filter.copyWith(active: value));
  }

  void _handleSearch() {
    widget.onChanged(widget.filter.copyWith(query: _searchController.text));
  }
}
