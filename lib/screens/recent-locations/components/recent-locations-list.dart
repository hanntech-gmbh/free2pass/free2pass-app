import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/appointment.dart';
import 'package:free2pass/models/merchant.dart';
import 'package:free2pass/screens/recent-locations/components/location-detail-tile/appointment-detail-tile.dart';
import 'package:free2pass/screens/recent-locations/components/location-detail-tile/location-detail-tile.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:latlong2/latlong.dart';

class RecentLocationsList extends StatelessWidget {
  final ScrollController controller;
  final LocationActivatedCallback onCardActivation;

  const RecentLocationsList(
      {Key? key, required this.controller, required this.onCardActivation})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // first the appointment listenable
    return ValueListenableBuilder(
        valueListenable: appointments.listenable(),
        builder: (c, Box<Appointment> appointmentsBox, w) {
          // now the visited location listenable
          return ValueListenableBuilder(
            valueListenable: visitedLocations.listenable(),
            builder: (c, Box<Merchant> visitedBox, w) {
              if (visitedBox.isEmpty && appointmentsBox.isEmpty) {
                return Material(
                  child: ListView(
                    controller: controller,
                    children: [
                      ListTile(
                        leading: Icon(Icons.info),
                        title: AlignedMarkdown(
                          alignment: CrossAxisAlignment.start,
                          data: S.of(context).seemsLikenyouHavenonpastCheckins,
                        ),
                      ),
                    ],
                  ),
                );
              }
              return Material(
                child: ListView.builder(
                  controller: controller,
                  padding: EdgeInsets.all(8.0),
                  itemBuilder: (c, i) {
                    // showing upcoming appointments first
                    if (i < appointmentsBox.length) {
                      final index = (appointmentsBox.length - 1) - i;
                      final Appointment appointment =
                          appointmentsBox.getAt(index)!;
                      return AppointmentDetailTile(
                        appointment: appointment,
                        onActivated: () => onCardActivation(
                          LatLng(
                            appointment.issuer.lat!,
                            appointment.issuer.lng!,
                          ),
                        ),
                      );
                    } else {
                      final Merchant location = visitedBox.getAt(
                          (visitedBox.length - 1) -
                              (i - appointmentsBox.length))!;
                      return LocationDetailTile(
                        location: location,
                        onActivated: () => onCardActivation(
                            LatLng(location.lat!, location.lng!)),
                      );
                    }
                  },
                  itemCount: visitedBox.length + appointmentsBox.length,
                ),
              );
            },
          );
        });
  }
}

typedef void LocationActivatedCallback(LatLng location);
