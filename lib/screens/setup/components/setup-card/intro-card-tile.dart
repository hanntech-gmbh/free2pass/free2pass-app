import 'package:flutter/material.dart';

class IntroCardTile extends StatelessWidget {
  final Widget child;

  const IntroCardTile({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final radius = BorderRadius.circular(8);
    return Card(
      elevation: 2,
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 4),
      shape: RoundedRectangleBorder(borderRadius: radius),
      child: ClipRRect(
        borderRadius: radius,
        child: Material(
          color: Colors.transparent,
          child: child,
        ),
      ),
    );
  }
}
