import 'package:flutter/material.dart';
import 'package:free2pass/components/default-drawer/default-drawer.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/screens/home/components/scan-certificate-button.dart';
import 'package:free2pass/screens/home/components/user-change-animation-delegate.dart';
import 'package:free2pass/screens/home/components/views/home-page-bottom-bar.dart';
import 'package:free2pass/screens/home/components/views/user-selector.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/home';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  AxisTransitionController _controller = AxisTransitionController();

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box>(
      valueListenable: users.listenable(),
      builder: (context, Box userBox, w) =>
          ValueListenableBuilder<Box<Certificate>>(
        valueListenable: certificates.listenable(),
        builder: (context, Box<Certificate> certBox, w) => Scaffold(
          appBar: DefaultAppBar(
            actions: [if (userBox.length > 1) UserSelector()],
          ),
          drawer: DefaultDrawer(),
          body: UserChangeAnimationDelegate(controller: _controller),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          floatingActionButton: ScanCertificateButton(
            page: _controller.current,
            changePage: _handleNavigation,
          ),
          bottomNavigationBar: (certBox.values
                  .where((element) => element.issuer?.isPrivate == true)
                  .isNotEmpty)
              ? HomePageBottomBar(
                  onNavigate: _handleNavigation,
                  page: _controller.current,
                )
              : null,
        ),
      ),
    );
  }

  void _handleNavigation(int value) {
    setState(() {
      _controller.current = value;
    });
  }
}
