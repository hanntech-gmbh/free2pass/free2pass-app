import 'package:flutter/material.dart';
import 'package:free2pass/generated/l10n.dart';
import 'package:free2pass/globals.dart';
import 'package:free2pass/models/occasion.model.dart';
import 'package:free2pass/screens/create-occasion/components/occasion-list-tile.dart';
import 'package:free2pass/screens/home/components/views/decoration/check-in-decoration.dart';
import 'package:free2pass_support/free2pass_support.dart';
import 'package:hive_flutter/hive_flutter.dart';

class CheckInView extends StatefulWidget {
  @override
  _CheckInViewState createState() => _CheckInViewState();
}

class _CheckInViewState extends State<CheckInView> {
  @override
  Widget build(BuildContext context) {
    return ResponsiveBox(
      child: LayoutBuilder(
        builder: (context, constraints) =>
            ValueListenableBuilder<Box<Occasion>>(
          valueListenable: createdOccasions.listenable(),
          builder: (context, Box<Occasion> box, w) {
            final decoration = CheckInDecoration();
            if (box.values.where((element) => element.isOngoing).isNotEmpty)
              return OccasionsScrollView(
                decoration: decoration,
                box: box,
                constraints: constraints,
              );
            else
              return decoration;
          },
        ),
      ),
    );
  }
}

class OccasionsScrollView extends StatelessWidget {
  final Widget decoration;
  final Box<Occasion> box;
  final BoxConstraints constraints;

  const OccasionsScrollView(
      {Key? key,
      required this.decoration,
      required this.box,
      required this.constraints})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          constraints:
              constraints.copyWith(maxHeight: constraints.maxHeight - 48),
          child: decoration,
        ),
        ListTile(
          leading: Icon(
            Icons.celebration,
            color: blue,
          ),
          title: Text(
            S.of(context).ongoingOccasions,
            style: Theme.of(context).textTheme.headline6,
          ),
        )
      ]
        ..addAll(
          box.values.where((element) => element.isOngoing).map(
                (e) => OccasionListTile(occasion: e),
              ),
        )
        ..add(Container(
          height: 64,
        )),
    );
  }
}
