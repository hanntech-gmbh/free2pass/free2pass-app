import 'package:free2pass/globals.dart';
import 'package:free2pass/models/certificate.dart';
import 'package:free2pass/utils/encode-personal-data.dart';

/// takes a certificate and ensures it is properly saved in the database
/// returns the user hash of the current certificate
Future<String> updateCertificateInDatabase(Certificate certificate) async {
  // preserving user hash of certificates
  String? hash;
  if (certificates.containsKey(certificate.id)) {
    // storing hash for further use
    final oldCertificate = certificates.get(certificate.id)!;
    hash = oldCertificate.userHash;

    // migrating missing data
    certificate.createDate ??= oldCertificate.createDate;
    certificate.issuer ??= oldCertificate.issuer;
    certificate.expectedValidity ??= oldCertificate.expectedValidity;
    certificate.expiryDate ??= oldCertificate.expiryDate;
    certificate.revokeReasonString ??= oldCertificate.revokeReasonString;
    certificate.name ??= oldCertificate.name;
    certificate.positiveComment ??= oldCertificate.positiveComment;
    certificate.dialogShown ??= oldCertificate.dialogShown;
    certificate.testType ??= oldCertificate.testType;
  }
  // if new certificate, adding hash or migration: if no certificate hash present, using default one
  if (hash == null) {
    final user = getCurrentUser();
    hash = user.hash;
  }
  // finally adding hash
  certificate.userHash = hash!;

  // saving certificate in database
  await certificates.put(certificate.id, certificate);
  return hash;
}
