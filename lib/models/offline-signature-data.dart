import 'package:flutter/foundation.dart';
import 'package:free2pass/api/f2p-api.dart';
import 'package:free2pass/models/user-data.dart';
import 'package:free2pass/utils/encode-personal-data.dart';
import 'package:free2pass_support/free2pass_support.dart';

/// data required to create a cryptographic signature for a certificate QR code
class OfflineSignatureData {
  final int? expiry;
  final String entropy;
  final String signature;
  final String salt;

  OfflineSignatureData._({
    this.expiry,
    required this.entropy,
    required this.signature,
    required this.salt,
  });

  /// creates a new [OfflineSignatureData] from a given [UserData] and
  /// [DateTime] expiry
  static Future<OfflineSignatureData?> generate({
    required UserData user,
    required DateTime expiry,
  }) async {
    final expiryInt = (expiry.millisecondsSinceEpoch / 1000).round();
    final entropy = Crypto.createRandomString(8);

    final handleString =
        '${user.name};${user.birthday};${user.zip};$expiryInt;$entropy';

    final salt = Crypto.getBCryptSalt();

    final params = [handleString, salt];

    final response = await F2PApi.instance.auth.signature(
      data: await (!kIsWeb
          ? compute<List<String>, String>(asyncHash, params)
          : asyncHash(params)),
    );

    if (!response.status.success) return null;

    return OfflineSignatureData._(
      expiry: expiryInt,
      signature: response.payload!,
      entropy: entropy,
      salt: salt,
    );
  }
}
