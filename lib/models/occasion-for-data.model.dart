import 'package:free2pass/models/occasion.model.dart';

class OccasionFormData {
  final bool success;
  final Future<Occasion>? occasionDelegate;
  final bool? autoCheckIn;

  OccasionFormData(
      {required this.success, this.occasionDelegate, this.autoCheckIn});
}
