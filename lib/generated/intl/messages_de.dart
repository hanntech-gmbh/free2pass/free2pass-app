// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a de locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'de';

  static String m0(x, y) => "Impfung ${x} von ${y}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "aPhotoDepictingYourFaceYourCamera":
            MessageLookupByLibrary.simpleMessage(
                "Ein Foto von deinem Gesicht oder deine Kamera"),
        "abortAndViewUsers": MessageLookupByLibrary.simpleMessage(
            "Abbrechen und Nutzer*innen ansehen"),
        "abortTest": MessageLookupByLibrary.simpleMessage("Test abbrechen"),
        "abortTestHelper": MessageLookupByLibrary.simpleMessage(
            "Das kann nicht rückgängig gemacht werden."),
        "abortTestLong": MessageLookupByLibrary.simpleMessage(
            "Möchtest du den Test wirklich abbrechen?"),
        "acceptDataCorrectFormatted": MessageLookupByLibrary.simpleMessage(
            "Ich bestätige hiermit die Richtigkeit meiner Daten. Bei Falschangaben kann free2**pass** nicht für den Nachweis von Testergebnissen dienen."),
        "acceptPrivacyPolicy": MessageLookupByLibrary.simpleMessage(
            "Ich akzeptiere die **Datenschutzerklärung**"),
        "acceptTermsOfUseFormatted": MessageLookupByLibrary.simpleMessage(
            "Ich akzeptiere die **Nutzungsbedingungen**"),
        "add": MessageLookupByLibrary.simpleMessage("Speichern"),
        "address": MessageLookupByLibrary.simpleMessage("Adresse"),
        "addressHelper":
            MessageLookupByLibrary.simpleMessage("Meldeadresse mit Hausnummer"),
        "addressValidator": MessageLookupByLibrary.simpleMessage(
            "Bitte gib eine Straße mit Hausnummer an."),
        "advancedDangerous":
            MessageLookupByLibrary.simpleMessage("Erweitert (Risiko)"),
        "advancedEncryption": MessageLookupByLibrary.simpleMessage(
            "Deine Daten werden mit erweiterter Verschlüsselung übertragen."),
        "advancedEncryptionLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Dein lokales Gesundheitsamt **unterstützt erweiterte Verschlüsselung**. Mit unserer einzigartigen Crypto-Technologie können wir **kryptografisch garantieren**, dass im Falle einer **Covid-19-Infektion ausschließlich** das **zuständige Gesundheitsamt** auf deine Daten zugreifen kann."),
        "appointmentCancelSuccessful":
            MessageLookupByLibrary.simpleMessage("Termin erfolgreich abgesagt"),
        "appointmentEnablePush": MessageLookupByLibrary.simpleMessage(
            "Push-Benachrichtigungen aktivieren"),
        "areYouSure":
            MessageLookupByLibrary.simpleMessage("Bist du dir sicher?"),
        "areYourConfidentWithThePhoto": MessageLookupByLibrary.simpleMessage(
            "Bist du zufrieden mit dem Foto?"),
        "back": MessageLookupByLibrary.simpleMessage("Zurück"),
        "birthday": MessageLookupByLibrary.simpleMessage("Geburtsdatum"),
        "birthdayValidator": MessageLookupByLibrary.simpleMessage(
            "Bitte gib einen Geburtstag an."),
        "cancel": MessageLookupByLibrary.simpleMessage("Abbrechen"),
        "cancelAppointment":
            MessageLookupByLibrary.simpleMessage("Termin absagen"),
        "cancelOtherReason":
            MessageLookupByLibrary.simpleMessage("Anderer Grund"),
        "cancelReasonLong": MessageLookupByLibrary.simpleMessage(
            "Warum möchtest du den Termin absagen?"),
        "cancelUnavailable":
            MessageLookupByLibrary.simpleMessage("Ich habe doch keine Zeit"),
        "cancelWrongTime": MessageLookupByLibrary.simpleMessage(
            "Ich habe eine falsche Uhrzeit / ein falsches Datum gewählt"),
        "certificate": MessageLookupByLibrary.simpleMessage("Testzertifikat"),
        "certificateId": MessageLookupByLibrary.simpleMessage("Test ID"),
        "certificateInvalid":
            MessageLookupByLibrary.simpleMessage("Zertifikat ungültig"),
        "certificateInvalidLong": MessageLookupByLibrary.simpleMessage(
            "Dieses Zertifikat\nscheint nicht für dich\nzu passen."),
        "certificateIssuer":
            MessageLookupByLibrary.simpleMessage("Herausgeberin"),
        "certificateRefreshError": MessageLookupByLibrary.simpleMessage(
            "Fehler beim Laden der Zertifikate. Bist du dir sicher, dass du mit dem Internet verbunden bist?"),
        "certificateRevoked": MessageLookupByLibrary.simpleMessage(
            "Das Ergebnis Deines letzten\nTests **wurde für ungültig erklärt**."),
        "certificateRevokedLong": MessageLookupByLibrary.simpleMessage(
            "Dein letzter Test wurde für ungültig erklärt"),
        "changeData": MessageLookupByLibrary.simpleMessage("Angaben ändern"),
        "changePersonalData":
            MessageLookupByLibrary.simpleMessage("Persönliche Angaben ändern"),
        "changeUser":
            MessageLookupByLibrary.simpleMessage("Nutzer*in wechseln"),
        "checkInIntroLongFormatted":
            MessageLookupByLibrary.simpleMessage("Eine kurze Einführung..."),
        "checkInLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Um die **Kontaktnachverfolgung**\nzu ermöglichen, kannst du an\nTeilnehmenden Locations mit einem\nnegativen Testergebnis digital\neinchecken.\n \n**Scan einfach den QR-Code vor Ort!**"),
        "checkInNow":
            MessageLookupByLibrary.simpleMessage("Jetzt **einchecken**"),
        "checkInSuccessful":
            MessageLookupByLibrary.simpleMessage("Erfolgreich eingecheckt!"),
        "checkOut": MessageLookupByLibrary.simpleMessage("Check-Out"),
        "checkin": MessageLookupByLibrary.simpleMessage("Check-In"),
        "checkinSuccessfulLong": MessageLookupByLibrary.simpleMessage(
            "Du kannst alle Check-Ins\nder letzten 14 Tage über\ndas Seitenmenü einsehen."),
        "chosePassportVerification": MessageLookupByLibrary.simpleMessage(
            "Okay, du möchtest lieber deinen Ausweis verwenden. Weiter im Text."),
        "clear": MessageLookupByLibrary.simpleMessage("Weg damit"),
        "close": MessageLookupByLibrary.simpleMessage("Schließen"),
        "closed": MessageLookupByLibrary.simpleMessage("Geschlossen"),
        "codeValidator": MessageLookupByLibrary.simpleMessage(
            "Bitte gib einen passenden Bestätigungscode ein."),
        "contactIssuer":
            MessageLookupByLibrary.simpleMessage("Testzentrum kontaktieren"),
        "continueTest": MessageLookupByLibrary.simpleMessage("Test fortsetzen"),
        "continueText": MessageLookupByLibrary.simpleMessage("Weiter"),
        "couldNotSavePhoto": MessageLookupByLibrary.simpleMessage(
            "Ich konnte das gewählte Foto leider nicht speichern."),
        "couldNotVerify":
            MessageLookupByLibrary.simpleMessage("Überprüfung fehlgeschlagen."),
        "createAnOccasion":
            MessageLookupByLibrary.simpleMessage("Veranstaltung erstellen"),
        "createAppointment":
            MessageLookupByLibrary.simpleMessage("Termin buchen"),
        "createNewOccasion": MessageLookupByLibrary.simpleMessage(
            "Neue **Veranstaltung** erstellen"),
        "createUser":
            MessageLookupByLibrary.simpleMessage("**Nutzer\\*in** hinzufügen"),
        "createUserDataLongFormatted": MessageLookupByLibrary.simpleMessage(
            "# Nutzer\\*in hinzufügen\nBitte gib die persönlichen Daten der Nutzer\\*in an"),
        "createUsersLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Lege weitere Nutzer\\*innen an\num z. B. für Familienmitglieder\nohne eigenes Handy Testzertifikate\nzu verwalten und sie an\nLocations einzuchecken."),
        "currentUser":
            MessageLookupByLibrary.simpleMessage("Aktuelle Nutzer*in"),
        "currentlyClosed":
            MessageLookupByLibrary.simpleMessage("Momentan geschlossen"),
        "currentlyOpened":
            MessageLookupByLibrary.simpleMessage("Momentan geöffnet"),
        "darkBeta": MessageLookupByLibrary.simpleMessage("Dunkel (Beta)"),
        "dateOfBirth": MessageLookupByLibrary.simpleMessage("Geburtsdatum"),
        "dateOfVaccination":
            MessageLookupByLibrary.simpleMessage("Datum der Impfung"),
        "day": MessageLookupByLibrary.simpleMessage("Tag"),
        "deleteCertificates":
            MessageLookupByLibrary.simpleMessage("Testergebnisse löschen"),
        "deleteUser": MessageLookupByLibrary.simpleMessage("Nutzer*in löschen"),
        "deleteUserLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Möchtest du wirklich die Nutzer\\*in und alle dazugehörigen Testergebnisse löschen? Das kann nicht rückgängig gemacht werden."),
        "directions": MessageLookupByLibrary.simpleMessage("Navigation"),
        "displayLocationLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Du kannst deine **Position auf der Karte anzeigen** lassen und uns dafür **Zugriff auf deinen ungefähren Standort** gewähren.\nDies kann dir helfen, teilnehmende Locations in deiner Nähe zu finden. Wenn du uns diese Berechtigung gewährst, wird der **Standortzuggriff auch während Check-Ins verwendet**, um sicherzustellen, dass du dich **tatsächlich an dem Ort befindest, an welchem du einchecken möchtest**.\nBeide Arten der Standortverarbeitung finden **ausschließlich lokal auf deinem Gerät** statt und unter **keinen Umständen wird dein Standort an irgendwen weitergegeben**."),
        "dontImport": MessageLookupByLibrary.simpleMessage("Verwerfen"),
        "duplicateUser":
            MessageLookupByLibrary.simpleMessage("Nutzer*in bereits vorhanden"),
        "duplicateUserLong": MessageLookupByLibrary.simpleMessage(
            "Für die angegebenen Daten existiert bereits eine Nutzer*in. Bitte gib andere Daten an oder kehre zur Nutzer*innenansicht zurück."),
        "editPhoneNumber":
            MessageLookupByLibrary.simpleMessage("Telefonnummer bearbeiten"),
        "editProvidedData":
            MessageLookupByLibrary.simpleMessage("Angegebene Daten ändern"),
        "editUser":
            MessageLookupByLibrary.simpleMessage("Nutzer*in bearbeiten"),
        "emailAddress":
            MessageLookupByLibrary.simpleMessage("Email-Adresse (Optional)"),
        "emailValidator": MessageLookupByLibrary.simpleMessage(
            "Angabe Optional - Diese Email-Adresse ist ungültig."),
        "encryptingUserData": MessageLookupByLibrary.simpleMessage(
            "Einen Augenblick bitte, wir verschlüsseln deine Daten."),
        "encryption": MessageLookupByLibrary.simpleMessage("Verschlüsselung"),
        "end": MessageLookupByLibrary.simpleMessage("Ende"),
        "enterVerificationCodeLong": MessageLookupByLibrary.simpleMessage(
            "Bitte gib den Bestätigungscode ein, den du per SMS erhalten hast. Dein Code ist 10 Minuten lang gültig."),
        "errorContactingIssuer": MessageLookupByLibrary.simpleMessage(
            "Keine Ahnung, wie ich das Testzentrum erreichen kann."),
        "errorCreatingAppointment": MessageLookupByLibrary.simpleMessage(
            "Fehler beim buchen des Slots."),
        "errorCreatingYourOccasion": MessageLookupByLibrary.simpleMessage(
            "Fehler beim Erstellen der Veranstaltung.\nWahrscheinlich unterstüzut deine\nKommune dies nicht."),
        "errorLoadingLocationData": MessageLookupByLibrary.simpleMessage(
            "Fehler beim Laden der Location"),
        "errorNoExpiryProvided": MessageLookupByLibrary.simpleMessage(
            "Fehler: Kein Ablaufzeitpunkt."),
        "errorRequestingVerificationCode": MessageLookupByLibrary.simpleMessage(
            "Ich konnte dir keinen Bestätigungscode senden. Bist du dir sicher, dass du deine Telefonnummer richtig angegeben hast?"),
        "errorSavingUserData": MessageLookupByLibrary.simpleMessage(
            "Fehler beim Speichern deiner Daten"),
        "errorWringVerificationCode": MessageLookupByLibrary.simpleMessage(
            "Ich konnte den Bestätigungscode nicht Überprüfen. Bitte überprüf nochmal deine Eingaben."),
        "expiredAt": MessageLookupByLibrary.simpleMessage("Abgelaufen am"),
        "expiredCertificates":
            MessageLookupByLibrary.simpleMessage("Test Archiv"),
        "filterLocations":
            MessageLookupByLibrary.simpleMessage("Locations filtern"),
        "finish": MessageLookupByLibrary.simpleMessage("Fertig"),
        "formatA4": MessageLookupByLibrary.simpleMessage("A4 (Standard)"),
        "formatA5": MessageLookupByLibrary.simpleMessage("A5"),
        "formatUSLegal": MessageLookupByLibrary.simpleMessage("US Formal"),
        "formatUSLetter": MessageLookupByLibrary.simpleMessage("US Brief"),
        "friday": MessageLookupByLibrary.simpleMessage("Freitag"),
        "from": MessageLookupByLibrary.simpleMessage("Von"),
        "fullName": MessageLookupByLibrary.simpleMessage("Vollständiger Name"),
        "fullNameHelper": MessageLookupByLibrary.simpleMessage(
            "Der Name, wie er auf deinem Ausweis steht"),
        "fullNameValidator": MessageLookupByLibrary.simpleMessage(
            "Bitte gib einen Vor- und Nachnamen an."),
        "fullyImmunized": MessageLookupByLibrary.simpleMessage(
            "Seit über zwei Wochen vollständig geimpft."),
        "generalVaccinationInformation":
            MessageLookupByLibrary.simpleMessage("Allgemeine Informationen"),
        "goBackHome":
            MessageLookupByLibrary.simpleMessage("Zurück zur Startseite"),
        "grant": MessageLookupByLibrary.simpleMessage("Gewähren"),
        "grantLocationAccess": MessageLookupByLibrary.simpleMessage(
            "Zugriff auf Standort gewähren"),
        "home": MessageLookupByLibrary.simpleMessage("Startseite"),
        "iUnderstood":
            MessageLookupByLibrary.simpleMessage("Zur Kenntnis genommen"),
        "identityCheck":
            MessageLookupByLibrary.simpleMessage("Identitätsprüfung"),
        "identityCheckLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Bitte zeige dem\nTestpersonal deinen\n**Personalausweis**."),
        "ifYouChangeYourPersonalDataAllPastCertificatesExpire":
            MessageLookupByLibrary.simpleMessage(
                "Wenn du deine persönlichen Angaben änderst, **erlischen all deine** vergangenen **Zertifikate**."),
        "ifYouChoseToUseYourPassportInsteadOfA":
            MessageLookupByLibrary.simpleMessage(
                "Wenn du deinen Ausweis statt eines Fotos verwenden möchtest, musst du bei jedem Check-In und Covid-19-Test deinen Personalausweis vorzeigen."),
        "ignore": MessageLookupByLibrary.simpleMessage("Ignorieren"),
        "imagePickError": MessageLookupByLibrary.simpleMessage(
            "Ich konnte da Bild nicht öffnen. Vielleicht versuchst du es mit einem Anderen?"),
        "importAddressFromInitialUser": MessageLookupByLibrary.simpleMessage(
            "Adresse von Hauptnutzer*in übernehmen"),
        "infoPrivateOccasion": MessageLookupByLibrary.simpleMessage(
            "Dies ist eine private Veranstaltung ohne Adressinformationen"),
        "information": MessageLookupByLibrary.simpleMessage("Informationen"),
        "initialUser": MessageLookupByLibrary.simpleMessage("Hauptnutzer*in"),
        "injection":
            MessageLookupByLibrary.simpleMessage("Potenziell schädliche Daten"),
        "injectionLong": MessageLookupByLibrary.simpleMessage(
            "Die von dir angegebenen Daten haben Ähnlichkeiten mit sog. Injections, einer Angriffsform, die häufig von Hacker*innen verwendet wird, um ungewollt Einfluss auf Software zu nehmen. Bitte sei dir darüber im Klaren, dass unsere Software gepentested wurde und wir alle Eingaben normalisieren.\nFalls du keine Hacker*in bist, kannst du diese Meldung ignorieren."),
        "internalCertificateFormatted": MessageLookupByLibrary.simpleMessage(
            "**Achtung:** internes Zertifikat"),
        "internalCertificateLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Unternehmenstest\nmit **ausschließlich**\n**interner** Gültigkeit."),
        "intro1Bottom": MessageLookupByLibrary.simpleMessage(
            "**Kein unnötiger Papierkram**\nbeim Einlass in Restaurants,\nEvent-Locations und vielem mehr!"),
        "intro1Head": MessageLookupByLibrary.simpleMessage(
            "free2**pass** ist Dein sicherer Ausweis\nfürs Smartphone, mit dem Du jederzeit\n**Zugriff auf Deinen aktuellen**\n**Covid-19 Test-Befund hast.**"),
        "intro2Bottom": MessageLookupByLibrary.simpleMessage(
            "Ein **Timer** zeigt Dir, wie lange Dich\nfree2**pass** noch zum Zugang\nder Location Deiner Wahl berechtigt."),
        "intro2Head": MessageLookupByLibrary.simpleMessage(
            "Scan einfach den **QR-Code in**\n**teilnehmenden Testzentren**\nund zack - schon hast du das Ergebnis\nauf Deinem Smartphone!"),
        "intro3Bottom": MessageLookupByLibrary.simpleMessage(
            "Scan den QR-Code vor Ort und\nDeine Daten werden im Falle eines\nInfektionsgeschehens **Ende-zu-Ende**\n**verschlüsselt** an das zuständige\nGesundheitsamt übermittelt."),
        "intro3Head": MessageLookupByLibrary.simpleMessage(
            "Du bist im Restaurant, Fitnessstudio\noder beim Friseur und musst einen Zettel\nzur Kontaktverfolgung ausfüllen?\n**Nicht mit free2pass!**"),
        "intro4Bottom": MessageLookupByLibrary.simpleMessage(
            "Du möchtest mehr erfahren?\nSchau in unsere **FAQ**."),
        "intro4Head": MessageLookupByLibrary.simpleMessage(
            "Datenschutz ist in einem digitalen\nZeitalter unser höchstes Gut.\nDarum ist free2**pass** **100%**\n**DSGVO-konform, manipulationssicher**\n**und mehrfach RSA-verschlüsselt.**"),
        "invalid": MessageLookupByLibrary.simpleMessage("Ungültig"),
        "invalidCheckInCode":
            MessageLookupByLibrary.simpleMessage("Ungültiger Check-In code"),
        "invalidCheckInCodeLong": MessageLookupByLibrary.simpleMessage(
            "Dieser Check-In code\nscheint nicht für\ndich zu passen."),
        "invalidCryptoQr":
            MessageLookupByLibrary.simpleMessage("Ungültiger Crypto QR"),
        "invalidCryptoQrLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Das scheint kein gültiger\nfree2**pass** Crypto QR zu sein.\nBist du dir sicher, dass du den richtigen\nCode gescannt hast?"),
        "invalidLocation": MessageLookupByLibrary.simpleMessage(
            "Du scheinst nicht an dem Ort zu sein, an dem du einchecken willst."),
        "invalidLocationLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Um Missbrauch vorzubeugen überprüfen wir, ob du an dem Ort bist, an dem du einchecken möchtest."),
        "issued": MessageLookupByLibrary.simpleMessage("Ausgestellt"),
        "issuer": MessageLookupByLibrary.simpleMessage("Testzentrum"),
        "issuerClosed": MessageLookupByLibrary.simpleMessage(
            "Das Testzentrum ist geschlossen."),
        "issuerRevokedAppointment": MessageLookupByLibrary.simpleMessage(
            "Das Testzentrum hat dich versetzt."),
        "issuerVerifiedFormatted":
            MessageLookupByLibrary.simpleMessage("Nächster **Schritt**"),
        "issuers": MessageLookupByLibrary.simpleMessage("Testzentren"),
        "keep": MessageLookupByLibrary.simpleMessage("Behalten"),
        "keepAvailable":
            MessageLookupByLibrary.simpleMessage("Bitte halte bereit:"),
        "language": MessageLookupByLibrary.simpleMessage("Sprache"),
        "later": MessageLookupByLibrary.simpleMessage("Später"),
        "learnMore": MessageLookupByLibrary.simpleMessage("Mehr erfahren"),
        "legalNotice": MessageLookupByLibrary.simpleMessage("Impressum"),
        "light": MessageLookupByLibrary.simpleMessage("Hell"),
        "locateMe":
            MessageLookupByLibrary.simpleMessage("Zeige meine Position an"),
        "locationRequired":
            MessageLookupByLibrary.simpleMessage("Standort benötigt"),
        "locationRequiredLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Um Missbrauch vorzubeugen\nbenötigen wir deinen\nStandort. Ansonsten wissen wir\nnicht, ob du wirklich dort bist\nwo du einchecken willst.\nDeine Position wird\nausschließlich lokal\nauf deinem Gerät verglichen."),
        "locationType": MessageLookupByLibrary.simpleMessage("Art des Ortes"),
        "locationTypes":
            MessageLookupByLibrary.simpleMessage("Arten der Locations"),
        "locationsInArea":
            MessageLookupByLibrary.simpleMessage("Locations in dem Umkreis:"),
        "lookingForQrCode":
            MessageLookupByLibrary.simpleMessage("Suche nach QR-Code..."),
        "matchingLocations":
            MessageLookupByLibrary.simpleMessage("Passende Locations"),
        "merchant":
            MessageLookupByLibrary.simpleMessage("Terilnehmende Location"),
        "merchants": MessageLookupByLibrary.simpleMessage("Händler*innen"),
        "monday": MessageLookupByLibrary.simpleMessage("Montag"),
        "month": MessageLookupByLibrary.simpleMessage("Monat"),
        "multiUser":
            MessageLookupByLibrary.simpleMessage("Weitere Nutzer*innen"),
        "name": MessageLookupByLibrary.simpleMessage("Name"),
        "nameAlreadySaved": MessageLookupByLibrary.simpleMessage(
            "Du hast mir bereits deinen Namen gesagt. Weiter im Text."),
        "negatie": MessageLookupByLibrary.simpleMessage("Negativ"),
        "negativeLong": MessageLookupByLibrary.simpleMessage(
            "Negativ auf Covid-19 getestet"),
        "newTestResultPending": MessageLookupByLibrary.simpleMessage(
            "Neues Testergebnis ausstehend"),
        "next": MessageLookupByLibrary.simpleMessage("Weiter"),
        "nextCertificate":
            MessageLookupByLibrary.simpleMessage("Nächster Test"),
        "nextDay": MessageLookupByLibrary.simpleMessage("Nächster Tag"),
        "no": MessageLookupByLibrary.simpleMessage("Nein"),
        "noConnectionLongFormatted": MessageLookupByLibrary.simpleMessage(
            "# Keine Internetverbindung\nEs scheint, als hast du keine\nInternetverbindung. Stelle bitte eine\nfunktionierende Verbindung her,\num fortzufahren."),
        "noCurrentCertificateLongFormatted":
            MessageLookupByLibrary.simpleMessage(
                "Es liegt **kein aktuelles**\n**Testergebnis** vor."),
        "noExpiredCertificatesLongFormatted":
            MessageLookupByLibrary.simpleMessage(
                "Du scheinst \n**keine vergangenen**\n**Zertifikate** zu haben."),
        "noInformation": MessageLookupByLibrary.simpleMessage("Keine Angabe"),
        "noMoreTests": MessageLookupByLibrary.simpleMessage(
            "Keine Testkits mehr verfügbar."),
        "noPhotoAvailable":
            MessageLookupByLibrary.simpleMessage("Kein Foto angegeben"),
        "noPhotoAvailableLong": MessageLookupByLibrary.simpleMessage(
            "Bitte Ausweis der Nutzer*in manuell prüfen"),
        "noRecentLocationsLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Du scheinst **keine**\n**vergangenen Check-Ins**\nzu haben."),
        "notAVaccinationCertificate": MessageLookupByLibrary.simpleMessage(
            "Das scheint kein passendes Impfzertifikat zu sein.."),
        "notFound":
            MessageLookupByLibrary.simpleMessage("404 - Nicht gefunden"),
        "notificationChannelTestResults":
            MessageLookupByLibrary.simpleMessage("Testergebnisse"),
        "notificationChannelTestResultsDetails":
            MessageLookupByLibrary.simpleMessage(
                "Informationen zu deinen neuen Covid-19-Testergebnissen"),
        "notificationChannelWebSocket":
            MessageLookupByLibrary.simpleMessage("Server-Verbindung"),
        "notificationChannelWebSocketLong": MessageLookupByLibrary.simpleMessage(
            "Diese stumme Benachrichtigung wird benötigt, damit wir im Hintergrund überprüfen können, ob ein neues Testergebnis für dich eingetroffen ist.."),
        "notificationResultAvailable": MessageLookupByLibrary.simpleMessage(
            "Dein Testergebnis ist verfügbar!"),
        "notificationSilentContent":
            MessageLookupByLibrary.simpleMessage("Warte auf Testergebnis..."),
        "oClock": MessageLookupByLibrary.simpleMessage("Uhr"),
        "occasionAutoCheckIn": MessageLookupByLibrary.simpleMessage(
            "Dich selbst automatisch einchecken"),
        "occasionNameLong": MessageLookupByLibrary.simpleMessage(
            "Bitte gibt der Veranstaltung einen Namen. Dieser wird allen Teilnehmer*innen angezeigt, jedoch niemals an das Gesundheitsamt oder Dritte übertragen."),
        "occasionTime": MessageLookupByLibrary.simpleMessage(
            "Bitte gib den Zeitraum deiner Veranstaltung an."),
        "okay": MessageLookupByLibrary.simpleMessage("Okay"),
        "ongoingOccasions": MessageLookupByLibrary.simpleMessage(
            "Deine laufenden Veranstaltungen:"),
        "onlyShowFree2passenablesLocations":
            MessageLookupByLibrary.simpleMessage(
                "Nur Locations mit Unterstützung von free2pass anzeigen"),
        "openPrivacyPolicy": MessageLookupByLibrary.simpleMessage(
            "Datenschutzerklärung anzeigen"),
        "openSourceLicenses":
            MessageLookupByLibrary.simpleMessage("Open-Source Lizenzen"),
        "openUsagePolicy": MessageLookupByLibrary.simpleMessage(
            "Nutzungsbedingungen anzeigen"),
        "openWebsite": MessageLookupByLibrary.simpleMessage("Website öffnen"),
        "openingHours": MessageLookupByLibrary.simpleMessage("Öffnungszeiten"),
        "optional": MessageLookupByLibrary.simpleMessage("Angabe Optional"),
        "otherUsers":
            MessageLookupByLibrary.simpleMessage("Andere Nutzer*innen"),
        "partiallyVaccinated":
            MessageLookupByLibrary.simpleMessage("Teilgeimpft."),
        "pass": MessageLookupByLibrary.simpleMessage("Pass"),
        "pdfBirth": MessageLookupByLibrary.simpleMessage("Geb.:"),
        "pdfCertificateSigned": MessageLookupByLibrary.simpleMessage(
            "Scanne den Crypto-QR,\n um die Echtheit des\nZertifikats zu prüfen!"),
        "pdfLegalNoTransfer": MessageLookupByLibrary.simpleMessage(
            "Die Verwendung des Testzertifikats ist ausschließlich für die oben genannte Person zulässig. Die Weitergabe oder Kopie des Dokuments ist nicht gestattet und führt unverzüglich zur Ungültigkeit des Zertifikats."),
        "pdfLegalNotice": MessageLookupByLibrary.simpleMessage(
            "free2pass GmbH | Hamburger Allee 2-4 | 30161 Hannover\nhello@free2pass.de | freepass.de"),
        "pdfLegalSignature": MessageLookupByLibrary.simpleMessage(
            "Dieses Schriftstück wurde maschinell erstellt und ist ohne Unterschrift gültig."),
        "pdfNegativeLong":
            MessageLookupByLibrary.simpleMessage("Testergebnis: NEGATIV"),
        "pdfNegativeTestLegal": MessageLookupByLibrary.simpleMessage(
            "Ein negatives Testergebnis bedeutet nicht, dass eine COVID-19-Infektion sicher ausgeschlossen werden kann. Das Ergebnis stellt lediglich den Gesundheitsstatus zum Zeitpunkt der Testdurchführung dar."),
        "pdfNoPhoto":
            MessageLookupByLibrary.simpleMessage("Kein Foto\nangegeben"),
        "pdfOfficial": MessageLookupByLibrary.simpleMessage("Offizielles"),
        "pdfPoCTest": MessageLookupByLibrary.simpleMessage(
            "PoC-Antigen-Test auf SARS-CoV-2"),
        "pdfTestCertificate":
            MessageLookupByLibrary.simpleMessage("Testzertifikat"),
        "pdfTestTime": MessageLookupByLibrary.simpleMessage("Testzeitpunkt: "),
        "pdfTestUsed": MessageLookupByLibrary.simpleMessage("Verwendeter Test"),
        "pdfUnknownTestType":
            MessageLookupByLibrary.simpleMessage("Unbekannter test typ"),
        "pdfValidUntil": MessageLookupByLibrary.simpleMessage("Gültig bis"),
        "pending": MessageLookupByLibrary.simpleMessage("Prüfung"),
        "pendingTestLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Das Ergebnis Deines letzten\nTests **steht noch aus**.\nSchau später vorbei, um\nzu sehen, ob dein\nErgebnis eingetroffen ist!"),
        "pendingTestResult":
            MessageLookupByLibrary.simpleMessage("Testergebnis ausstehend"),
        "pendingTestResultLong": MessageLookupByLibrary.simpleMessage(
            "Das Ergebnis Deines\nletzten Testes steht\nnoch aus. Wir informieren\nDich, sobald das\nErgebnis eingetroffen ist!"),
        "permanentlyRemove":
            MessageLookupByLibrary.simpleMessage("Dauerhaft löschen"),
        "personalDataLongFormatted": MessageLookupByLibrary.simpleMessage(
            "# Persönliche Angaben\nWir benötigen deine persönlichen Angaben, um sicherzustellen, dass ein Covid-19 Test-Zertifikat tatsächlich nur von der Person verwendet wird, der es ausgestellt wurde. Auch hier werden deine Daten nur lokal auf deinem Gerät gespeichert und bei Bedarf Ende-zu-Ende-verschlüsselt an die Covid-19-Teststationen übertragen."),
        "phoneAlreadyVerifiedLong": MessageLookupByLibrary.simpleMessage(
            "Deine Telefonnummer kenne ich schon. Weiter im Text."),
        "phoneNumberInputHelper": MessageLookupByLibrary.simpleMessage(
            "Die internationale Vorwal ist optional."),
        "phoneNumberLongFormatted": MessageLookupByLibrary.simpleMessage(
            "# Handynummer\nWir benötigen deine Handynummer, um einmal zu überprüfen, dass du auch eine real erreichbare Person bist. Diese wird nicht bei uns abgespeichert, sondern lediglich lokal auf deinem Gerät gespeichert. Wir können daher keine Informationen über dich zu deiner Person zuordnen."),
        "phoneNumberLostDescription": MessageLookupByLibrary.simpleMessage(
            "Leider gab es in einer vorherigen Version von free2pass einen Fehler, wodurch die Telefonnummer verloren gehen konnte. Sie müssen bitte Ihre Telefonnummer im nächsten Schritt neu verifizieren."),
        "phoneNumberLostTitle":
            MessageLookupByLibrary.simpleMessage("Telefonnummer verloren"),
        "phoneNumberValidator": MessageLookupByLibrary.simpleMessage(
            "Bitte gib eine gültige Telefonnummer ein."),
        "photoAlreadySaved": MessageLookupByLibrary.simpleMessage(
            "Du scheinst schon ein Foto gespeichert zu haben. Weiter im Text."),
        "photoValidator":
            MessageLookupByLibrary.simpleMessage("Bitte Foto auswählen"),
        "pin": MessageLookupByLibrary.simpleMessage("PIN"),
        "pleaseProvideAValidTown":
            MessageLookupByLibrary.simpleMessage("Bitte gib einen Wohnort an."),
        "pleaseProvideAValidZipCode": MessageLookupByLibrary.simpleMessage(
            "Bitte gib eine Postleitzahl an."),
        "pleaseTellMeYourName":
            MessageLookupByLibrary.simpleMessage("Bitte dag mir deinen Namen."),
        "positive": MessageLookupByLibrary.simpleMessage("Positiv"),
        "positiveComment":
            MessageLookupByLibrary.simpleMessage("Wichtiger Hinweis"),
        "positiveLong": MessageLookupByLibrary.simpleMessage(
            "Positiv auf Covid-19 getestet"),
        "positiveTestNotice": MessageLookupByLibrary.simpleMessage(
            "Dein Testergebnis wird automatisch\nan das zuständige Gesundheitsamt übermittelt.\n**Bitte halte Dich an die geltenden**\n**[Quarantäne-Bestimmungen](https://www.infektionsschutz.de/coronavirus/tests-auf-sars-cov-2/antigen-schnelltest.html#c14955)!**"),
        "poweredByOpenstreetmap":
            MessageLookupByLibrary.simpleMessage("Daten von OpenStreetMap"),
        "previousCertificate":
            MessageLookupByLibrary.simpleMessage("Vorheriger Test"),
        "previousDay": MessageLookupByLibrary.simpleMessage("Vorheriger Tag"),
        "privacyPolicy":
            MessageLookupByLibrary.simpleMessage("Datenschutzerklärung"),
        "privateOccasion":
            MessageLookupByLibrary.simpleMessage("Private Veranstaltung"),
        "providePhoneNumberLong": MessageLookupByLibrary.simpleMessage(
            "Wir benötigen deine Handynummer zur Verifizierung deines Geräts. Du erhältst anschließend per SMS einen Code, um dein Smartphone mit free2pass zu verknüpfen.\n**Hinweis**: Seit dem letzten Update haben wir das Sicherheitskonzept nochmal verbessert. Daher musst du nun leider deine Telefonnummer nochmals bestätigen."),
        "pushNotifications":
            MessageLookupByLibrary.simpleMessage("Push-Benachrichtigungen"),
        "pushNotificationsEnabledTapToDisable":
            MessageLookupByLibrary.simpleMessage(
                "Push-Benachrichtigungen aktiviert. Hier tippen zum deaktivieren."),
        "pushPrivacyNotice": MessageLookupByLibrary.simpleMessage(
            "Du kannst Push-Benachrichtigungen zu abgesagten Terminen und verfügbaren Testergebnissen erhalten. Bitte beachte, dass dies deine Anonymität gefährdet."),
        "quitApp": MessageLookupByLibrary.simpleMessage("App verlassen"),
        "readyToGo": MessageLookupByLibrary.simpleMessage("Los geht\'s!"),
        "reason": MessageLookupByLibrary.simpleMessage("Grund"),
        "recentLocations": MessageLookupByLibrary.simpleMessage("Locations"),
        "refresh": MessageLookupByLibrary.simpleMessage("Aktualisieren"),
        "remove": MessageLookupByLibrary.simpleMessage("Entfernen"),
        "removeVaccinationCertificate":
            MessageLookupByLibrary.simpleMessage("Impfzertifikat entfernen"),
        "removeVaccinationCertificateLongFormatted":
            MessageLookupByLibrary.simpleMessage(
                "Bist du dir sicher, dass du folgendes Impfzertifikat permanent von deinem Gerät löschen möchtest:"),
        "requestNewCode":
            MessageLookupByLibrary.simpleMessage("Neuen Code anfordern"),
        "requestNewTestResult":
            MessageLookupByLibrary.simpleMessage("Neues Ergebnis abfragen"),
        "requestVerificationCode":
            MessageLookupByLibrary.simpleMessage("Bestätigungscode anfordern"),
        "reset": MessageLookupByLibrary.simpleMessage("Löschen"),
        "resetAppLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Du setzt alle App-Einstellungen, sowie deine persönlichen Angaben zurück. Zusätzlich erlischen **alle Zertifikate, Check-Ins und Kontakt-Nachverfolgungsdaten**."),
        "resetFilters":
            MessageLookupByLibrary.simpleMessage("Filter zurücksetzen"),
        "resetFree2passFormatted":
            MessageLookupByLibrary.simpleMessage("free2**pass** zurücksetzen"),
        "resetRequired":
            MessageLookupByLibrary.simpleMessage("Zurücksetzen erforderlich"),
        "restartSetup":
            MessageLookupByLibrary.simpleMessage("Einrichtung neu starten"),
        "retry": MessageLookupByLibrary.simpleMessage("Wiederholen"),
        "returnHome": MessageLookupByLibrary.simpleMessage("Zur Startseite"),
        "saturday": MessageLookupByLibrary.simpleMessage("Samstag"),
        "save": MessageLookupByLibrary.simpleMessage("Speichern"),
        "scanCheckInCode":
            MessageLookupByLibrary.simpleMessage("Check-In-Code scannen"),
        "scanNewTest":
            MessageLookupByLibrary.simpleMessage("Neuen Test **scannen**"),
        "scanVaccinationCertificate": MessageLookupByLibrary.simpleMessage(
            "Offizielles Impfzertifikat scannen"),
        "search": MessageLookupByLibrary.simpleMessage("Suchen"),
        "seemsLikenyouHavenonpastCheckins":
            MessageLookupByLibrary.simpleMessage(
                "Du scheinst **keine vergangenen Check-Ins** zu haben."),
        "selectPhoto": MessageLookupByLibrary.simpleMessage("Foto wählen"),
        "selectPhotoLongFormatted": MessageLookupByLibrary.simpleMessage(
            "# Foto wählen\nDu kannst ein Foto hinterlegen, um den **Identitätsabgleich** in Covid-19-Testzentren und teilnehmenden Locations zu **vereinfachen**. Hierfür benötigen wir die **Berechtigung, ein Foto aus der Galerie**, als **Datei dem internen Speicher** oder per **Zugriff auf die Kamera** zu importieren. Wenn du uns erlaubst, auf die Kamera zuzugreifen, erlaubt dir das auch, später **QR-Codes für Covid-19-Tests oder Check-Ins** in teilnehmenden Locations zu scannen. Unter **keinen Umständen** wird dieses Foto an uns oder Dritte **übertragen**. Wenn du kein Foto hinterlegen möchtest, kannst du alternativ auch in Locations oder Testzentren deinen **Personalausweis zur Identitätsprüfung** vorzeigen."),
        "selectSelfie":
            MessageLookupByLibrary.simpleMessage("Selfie aus Galerie wählen"),
        "selectSlot": MessageLookupByLibrary.simpleMessage("Slot auswählen"),
        "selectThisSlot":
            MessageLookupByLibrary.simpleMessage("Diesen Slot wählen"),
        "settings": MessageLookupByLibrary.simpleMessage("Einstellungen"),
        "shareAsPdf": MessageLookupByLibrary.simpleMessage("Als PDF teilen"),
        "showDeviceTestStation": MessageLookupByLibrary.simpleMessage(
            "Bitte zeige dein Gerät dem Testpersonal."),
        "showLastVisitedFormatted": MessageLookupByLibrary.simpleMessage(
            "Alle **besuchten Orte**\nabrufen"),
        "showPassport": MessageLookupByLibrary.simpleMessage("Pass anzeigen"),
        "standardEncryption": MessageLookupByLibrary.simpleMessage(
            "Deine Daten werden mit einfacher Verschlüsselung übertragen."),
        "standardEncryptionLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Leider **widersetzen sich einige Gesundheitsämter** gegenüber jeder Form von **Datensicherheit**. In diesem Falle können wir leider nicht unsere erweiterte Verschlüsselung verwenden. Obwohl dadurch eine **Sicherheitsschicht wegfällt**, bleiben deine Daten immer noch **Ende-zu-Ende-verschlüsselt**. Bitte **wende dich an die zuständigen Lokalbehörden**, um auf erweiterte Verschlüsselung umzusteigen."),
        "start": MessageLookupByLibrary.simpleMessage("Beginn"),
        "startScanVaccinationCertificate":
            MessageLookupByLibrary.simpleMessage("Impfzertifikat scannen"),
        "startTestNow":
            MessageLookupByLibrary.simpleMessage("Test jetzt starten"),
        "store": MessageLookupByLibrary.simpleMessage("Speichern"),
        "successfullyCheckedIn":
            MessageLookupByLibrary.simpleMessage("Erfolgreich eingecheckt."),
        "successfullyReservedSlot":
            MessageLookupByLibrary.simpleMessage("Slot erfolgreich gebucht!"),
        "sunday": MessageLookupByLibrary.simpleMessage("Sonntag"),
        "takeSelfie": MessageLookupByLibrary.simpleMessage("Selfie aufnehmen"),
        "tapToEnlarge":
            MessageLookupByLibrary.simpleMessage("Zum Vergrößern klicken."),
        "tapToShowQr":
            MessageLookupByLibrary.simpleMessage("Crypto-QR anzeigen"),
        "termsOfService":
            MessageLookupByLibrary.simpleMessage("Nutzungsbedingungen"),
        "testCenter": MessageLookupByLibrary.simpleMessage("Testzentrum"),
        "testedLong":
            MessageLookupByLibrary.simpleMessage("Auf Covid-19 getestet"),
        "thatsNotAMatchingQrCode": MessageLookupByLibrary.simpleMessage(
            "Das ist kein passender QR-Code."),
        "theRequestedPageWasNotFound": MessageLookupByLibrary.simpleMessage(
            "Die erfragte Seite konnte nicht gefunden werden."),
        "theme": MessageLookupByLibrary.simpleMessage("Thema"),
        "themeDefault": MessageLookupByLibrary.simpleMessage("Standard"),
        "thursday": MessageLookupByLibrary.simpleMessage("Donnerstag"),
        "time": MessageLookupByLibrary.simpleMessage("Uhrzeit"),
        "timezone": MessageLookupByLibrary.simpleMessage("timezone"),
        "to": MessageLookupByLibrary.simpleMessage("Bis"),
        "today": MessageLookupByLibrary.simpleMessage("Heute,"),
        "toggleFlash": MessageLookupByLibrary.simpleMessage("Blitz umschalten"),
        "tooManyAppointments":
            MessageLookupByLibrary.simpleMessage("Zu viele Termine."),
        "tooManyAttempts":
            MessageLookupByLibrary.simpleMessage("Zu viele Versuche"),
        "tooManyAttemptsLong": MessageLookupByLibrary.simpleMessage(
            "Um Missbrauch vorzubeugen, sind die Registrierungsversuche beschränkt. Bitte warte einen Augenblick und versuch es erneut."),
        "town": MessageLookupByLibrary.simpleMessage("Ort"),
        "tuesday": MessageLookupByLibrary.simpleMessage("Dienstag"),
        "tutorial": MessageLookupByLibrary.simpleMessage("So **geht\'s**"),
        "uniqueCertificateIdentifier": MessageLookupByLibrary.simpleMessage(
            "Eindeutige Zertifikatkennung"),
        "unknownReason":
            MessageLookupByLibrary.simpleMessage("Unbekannter Grund."),
        "upcomingAppointment":
            MessageLookupByLibrary.simpleMessage("Kommender Termin"),
        "updateAvailable":
            MessageLookupByLibrary.simpleMessage("Update verfügbar"),
        "updateMandatoryLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Dieses Update ist essenziell. Du kannst fre2pass ohne dieses nicht weiter verwenden."),
        "updateNow":
            MessageLookupByLibrary.simpleMessage("Jetzt aktualisieren"),
        "updateOptionalLongFormatted": MessageLookupByLibrary.simpleMessage(
            "Wir empfehlen dir, free2pass so bald wie möglich zu aktualisieren. Wenn du das gerade nicht kannst, ist dies auch später noch möglich."),
        "usePassport":
            MessageLookupByLibrary.simpleMessage("Ausweis verwenden"),
        "usePassportLong": MessageLookupByLibrary.simpleMessage(
            "In diesem falle musst du bei jedem Check-In und Covid-19-Test deinen Personalausweis vorzeigen."),
        "vaccinatedQuestion": MessageLookupByLibrary.simpleMessage(
            "Bist du gegen Covid-19 geimpft?"),
        "vaccinatedQuestionExplanation": MessageLookupByLibrary.simpleMessage(
            "Du erhältst durch die Angabe keinen Vorteil. Wir sind aus statistischen Gründen dazu verpflichtet, dich zu fragen."),
        "vaccinationCenter":
            MessageLookupByLibrary.simpleMessage("Impfzentrum"),
        "vaccinationCertificate":
            MessageLookupByLibrary.simpleMessage("Impfzertifikate"),
        "vaccinationCertificateTitle": MessageLookupByLibrary.simpleMessage(
            "Offizielles COVID-19 Impfzertifikat"),
        "vaccinationCertificateVerificationNotice":
            MessageLookupByLibrary.simpleMessage(
                "Achtung: Dieses Impfzertifikat ist nür gültig, wenn dieses digital überprüft wurde."),
        "vaccinationInformationParagraphs": MessageLookupByLibrary.simpleMessage(
            "Bitte beachte, dass ein digitales Impfzertifikat nicht automatisch zum Reisen innerhalb der Europäischen Union berechtigt. Weitere Informationen zum aktuellen Infektionsgeschehen und den gelltenden Reisebestimmungen in der Europäischen Union findest du unter [reopen.europa.eu](https://reopen.europa.eu/).  \nHinweise zum digitalen Impfzertifikat findest du unter [www.digitaler-impfnachweis-app.de](https://www.digitaler-impfnachweis-app.de/)."),
        "vaccinationNumber": m0,
        "vaccinationStateImmunized":
            MessageLookupByLibrary.simpleMessage("**Zweimal**"),
        "vaccinationStateImmunizedLong": MessageLookupByLibrary.simpleMessage(
            "Ich bin **vollständig** geimpft"),
        "vaccinationStateNone":
            MessageLookupByLibrary.simpleMessage("**Noch nicht**"),
        "vaccinationStateNoneLong": MessageLookupByLibrary.simpleMessage(
            "Ich wurde noch **gar nicht** geimpft"),
        "vaccinationStatePartial":
            MessageLookupByLibrary.simpleMessage("**Einmal**"),
        "vaccinationStatePartialLong": MessageLookupByLibrary.simpleMessage(
            "**Partiell** - Ich wurde einmal geimpft und **erwarte noch eine Impfung**"),
        "vaccinationStations":
            MessageLookupByLibrary.simpleMessage("Impfstationen"),
        "validVaccinationCertificateFound":
            MessageLookupByLibrary.simpleMessage(
                "Gültiges Impfzertifikat gefunden."),
        "validatorOccasionName": MessageLookupByLibrary.simpleMessage(
            "Bitte gib deiner Veranstaltung einen passenden Namen."),
        "validatorOccasionRange": MessageLookupByLibrary.simpleMessage(
            "Bitte wähl den Zeitraum der Veranstaltung"),
        "validity": MessageLookupByLibrary.simpleMessage("Gültig bis"),
        "verificationSuccessful":
            MessageLookupByLibrary.simpleMessage("Überprüfung erfolgreich"),
        "verify": MessageLookupByLibrary.simpleMessage("Überprüfen"),
        "version": MessageLookupByLibrary.simpleMessage("Version"),
        "waitingForFullProtection": MessageLookupByLibrary.simpleMessage(
            "Vollständig geimpft und wartet zweiwöchige Wirkungszeit ab."),
        "weEncryptYourData": MessageLookupByLibrary.simpleMessage(
            "Wir verschlüsseln deine Daten..."),
        "webVaccinationCertificate": MessageLookupByLibrary.simpleMessage(
            "For technical reasons, the digital vaccination certificate is unfortunately only available in the native mobile app."),
        "website": MessageLookupByLibrary.simpleMessage("Website"),
        "wednesday": MessageLookupByLibrary.simpleMessage("Mittwoch"),
        "welcomeToFree2pass":
            MessageLookupByLibrary.simpleMessage("Willkommen in free2pass!"),
        "work": MessageLookupByLibrary.simpleMessage("Arbeit"),
        "year": MessageLookupByLibrary.simpleMessage("Jahr"),
        "yes": MessageLookupByLibrary.simpleMessage("Ja"),
        "youCanceledAppointment": MessageLookupByLibrary.simpleMessage(
            "Du hast den Termin platzen lassen."),
        "youChangedImportantUserDataIfYouProceedAllCertificates":
            MessageLookupByLibrary.simpleMessage(
                "Du hast die persönlichen Angaben der Nutzer*in geändert. Wenn du diese speicherst, gehen alle vergangenen Testergebnisse der Nutzer*in verloren. Möchtest du fortfahren?"),
        "yourPhoneNumber":
            MessageLookupByLibrary.simpleMessage("Deine Telefonnummer"),
        "yourVerificationCode":
            MessageLookupByLibrary.simpleMessage("Dein Bestätigungscode"),
        "zipCode": MessageLookupByLibrary.simpleMessage("Postleitzahl")
      };
}
